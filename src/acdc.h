/*
    Copyright 2014 Rodrigo Jardim da Fonseca
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef ACDC__h
#define ACDC__h

#ifdef __cplusplus
extern "C"
{
#endif

#define DEFAULT_CONFIG "acdc.ini"

// Type to store errors
typedef struct {
    const char *type; // String identifier to be used by clients to handle errors
    const char *message; // A comprehensive message
} error_t;

// Possible errors
const error_t INVALID_URL = {"INVALID_URL", "Could not extract the domain from the URL. Is this a valid URL?"};
const error_t UNKOWN_DOMAIN = {"UNKOWN_DOMAIN","Unknown domain. Please configure the extractors for this domain."};
const error_t HTTP_NOT_OK = {"HTTP_NOT_OK", "Get page failed. Is it accessible?"};
const error_t EXTRACTION_ERROR = {"EXTRACTION_ERROR", "One or more fields could not be extracted."};
const error_t UNKOWN_ERROR = {"UNKOWN_ERROR", "OMG! This error should never happen."};

// Type for hashing the domains
typedef unsigned long hash_t;

// Structure to store a string and it's length
typedef struct string {
  char *ptr;
  size_t len;
} string_t;

// Structure to store key-value pair
typedef struct tuple {
    char *name;
    char *value;
} tuple_t;

// A structure to store a extractor definition
typedef struct extractor {
    hash_t h;              // Domain hash (used for comparison to match extractors)
    char *domain;          // The domain this extractor is configured to
    tuple_t *xpaths;       // List of key-value pairs where the key is the name of the field and the values the xpath that extracts that field
    size_t xpaths_len;     // Length of the above array

} extractor_t;

// A list of extractor (see above)
typedef struct extractors {
    extractor_t **extractors;
    size_t len;
} extractors_t;

// Do all the magic
char * collect(char *url, extractors_t *extractors);
// Print program usage
void usage(const char *name);
// Trim whitespaces
size_t trimwhitespace(char *out, size_t len, const char *str);
// Callback handler for libcurl writes the content on a string
size_t writefunc(void *ptr, size_t size, size_t nmemb, struct string *s);
// Return the proper extractor based on the domain
extractor_t *get_extractor(extractors_t* extractors, char *domain);
// Get remote page as string and return the response code
long get_html_page(char* url, string_t* content);
// Returns a hash of a string
hash_t hash(const char *str);
// Initialize string structure
void inistring_t(struct string *s);
// Extract data from a page as a JSON Object
cJSON * extract_json_data(char *url, string_t *page, extractor_t *extractor);
// Run the console mode
void console_mode(extractors_t *extractors, char *url);
// Run the socket mode
void socket_mode(extractors_t *extractors, unsigned short port);
// Receive a full URL and returns the domain
char* get_domain(char* url);
// Used by the configuration parser
int config_handler(void* extractors, const char* section, const char* name, const char* value);

#ifdef __cplusplus
}
#endif

#endif