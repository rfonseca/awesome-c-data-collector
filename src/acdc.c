/*
    Copyright 2014 Rodrigo Jardim da Fonseca
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <curl/curl.h>
#include <libxml/HTMLparser.h>
#include <libxml/xpath.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include "ini.h"
#include "cJSON.h"
#include "url_parser.h"
#include "acdc.h"

int main(int argc, char **argv) {
    char c;
    int errflg = 0;
    int uflag = 0;
    int sflag = 0;
    char *config = NULL;
    char *url = NULL;
    unsigned int port;

    /* Parse command line arguments */
    while ((c = getopt(argc, argv, "hf:u:s:")) != -1) {
        switch(c) {
        case 'h': // -h Show help and exit
            usage(argv[0]);
            exit(EXIT_SUCCESS);
        case 'f': // -f <filename> Set the configuration file name
            config = optarg;
            break;
        case 'u': // -u <url> Set URL for data extractions (console mode)
            if (sflag) { // Can't work along with socket mode
                errflg++;
            } else {
                url = optarg;
                uflag++;
            }
            break;
        case 's': // -s <port> Starts socket mode at port
            if (uflag) { // Can't work along with console mode
                errflg++;
            } else {
                sflag++;
                port = atoi(optarg);            }
            break;
        case ':':
            fprintf(stderr, "Error: Option -%c requires an operand\n", optopt);
            errflg++;
            break;
        case '?':
            fprintf(stderr, "Error: Unrecognized option: '-%c'\n", optopt);
            errflg++;
            break;
        }
    }
    
    if (errflg) {
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    // Assume default configuration file
    if (config == NULL) {
        config = DEFAULT_CONFIG;
    }

    // Load configuration file
    extractors_t extractors;
    extractors.len = 0;
    extractors.extractors = NULL;
    if (ini_parse(config, config_handler, &extractors) < 0) {
        fprintf(stderr, "Can't load configuration at '%s'\n", config);
        exit(EXIT_FAILURE);
    }

    // Switch mode
    if (uflag) { // Console mode
        console_mode(&extractors, url);
    } else if (sflag) { // Socket mode
        socket_mode(&extractors, port);
    } else { // Mode required (error)
        fprintf(stderr, "Please choose a operation mode.\n");
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    return 0;
}

// Hash from: http://www.cse.yorku.ca/~oz/hash.html#djb2
hash_t hash(const char *str) {
    unsigned long hash = 5381;
    int c;
    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    return hash;
}

char* get_domain(char* url) {
    struct parsed_url* u = parse_url(url);
    if (u == NULL) return NULL;
    char *d = strdup(u->host);
    parsed_url_free(u);
    return d;
}

int config_handler(void* extractors, const char* section, const char* name, const char* value) {
    extractors_t *exts = (extractors_t*) extractors;
    extractor_t *e = NULL;
    hash_t h = hash(section);
    int i;
    for (i = 0; i < exts->len; i++ ) {
        if (exts->extractors[i]->h == h) {
            e = exts->extractors[i];
            break;
        }
    }
    if (e == NULL) {
        exts->extractors = realloc(exts->extractors, sizeof(extractor_t*)*(exts->len+1));
        exts->extractors[exts->len] = malloc(sizeof(extractor_t));
        e = exts->extractors[exts->len];
        e->h = h;
        e->domain = strdup(section);
        e->xpaths = malloc(sizeof(tuple_t)*10);
        e->xpaths_len = 0;
        exts->len++;
    }
    tuple_t *t = malloc(sizeof(tuple_t));
    t->name = strdup(name);
    t->value = strdup(value);
    e->xpaths[e->xpaths_len] = *t;
    e->xpaths_len = e->xpaths_len + 1;
    return 1;
}

extractor_t *get_extractor(extractors_t* extractors, char *domain) {
    hash_t h = hash(domain);

    extractor_t* e = NULL;
    int i;
    for (i = 0; i < extractors->len; i++ ) {
        if (extractors->extractors[i]->h == h) {
            e = extractors->extractors[i];
            break;
        }
    }
    return e;
}

size_t trimwhitespace(char *out, size_t len, const char *str) {
    if(len == 0) return 0;

    const char *end;
    size_t out_size;

    // Trim leading space
    while(isspace(*str)) str++;

    if(*str == 0) { // All spaces?
        *out = 0;
        return 1;
    }

    // Trim trailing space
    end = str + strlen(str) - 1;
    while(end > str && isspace(*end)) end--;
    end++;

    // Set output size to minimum of trimmed string length and buffer size minus 1
    out_size = (end - str) < len-1 ? (end - str) : len-1;

    // Copy trimmed string and add null terminator
    memcpy(out, str, out_size);
    out[out_size] = 0;

    return out_size;
}

void usage(const char *name) {
    assert(name);
    fprintf(stderr, "\nUsage: %s", name);
    fprintf(stderr, " [-f <file>]");
    fprintf(stderr, " [-u <site>]");
    fprintf(stderr, " [-s <port>]");
    fprintf(stderr, "\n\nOptions:\n");
    fprintf(stderr, " -f <file>    Use the specified configuration file.\n");
    fprintf(stderr, " -u <site>    Console mode: Collect data from the specified URL.\n");
    fprintf(stderr, " -s <port>    Socket mode: Start the program at the socket mode listening in the specified port.\n");
    fprintf(stderr, "\n");
    fprintf(stderr, "Console Mode:\nThe program receives a URL by a command line argument and prints the result on the standard output.\n");
    fprintf(stderr, "\n");
    fprintf(stderr, "Socket Mode:\nThe program await for connections in the specified port. The program expects a URL form where to collect the data and then sends the collected data back as a JSON. The connection is kept open listening indefinitely to new URLs until the client closes it. Each instance of the program responds to a single request at a time. Multiple connections are queued.\n");
    fprintf(stderr, "\n\n");
}

void inistring_t(struct string *s) {
  s->len = 0;
  s->ptr = malloc(s->len+1);
  if (s->ptr == NULL) {
    fprintf(stderr, "malloc() failed\n");
    exit(EXIT_FAILURE);
  }
  s->ptr[0] = '\0';
}

size_t writefunc(void *ptr, size_t size, size_t nmemb, struct string *s) {
    size_t new_len = s->len + size*nmemb;
    s->ptr = realloc(s->ptr, new_len+1);
    if (s->ptr == NULL) {
        fprintf(stderr, "realloc() failed\n");
        exit(EXIT_FAILURE);
    }
    memcpy(s->ptr+s->len, ptr, size*nmemb);
    s->ptr[new_len] = '\0';
    s->len = new_len;

    return size*nmemb;
}

long get_html_page(char* url, string_t* content) {
    CURL *curl = NULL;

    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, content);
        if (curl_easy_perform(curl) == CURLE_OK) {
            long response_code = NULL;
            if (curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code) == CURLE_OK){
                curl_easy_cleanup(curl);
                return response_code;
            } else {
                perror("Error: Failed to get response code from curl.");
            }
        } else {
            perror("Error: Failed to perform GET.");
        }
    }
    return -1;
}

cJSON * extract_json_data(char *url, string_t *page, extractor_t *extractor) {
    // Parse page
    htmlDocPtr htmlDoc;
    htmlDoc = htmlReadMemory(page->ptr, page->len, url, NULL, HTML_PARSE_NOERROR);
    xmlXPathContextPtr context = xmlXPathNewContext(htmlDoc);
    
    // Initialize response
    cJSON *resp, *data, *error = NULL, *error_data = NULL;
    resp = cJSON_CreateObject();
    cJSON_AddStringToObject(resp, "url", url);
    data =  cJSON_CreateObject();
    cJSON_AddItemToObject(resp, "data", data);
    

    // Extract data
    size_t i = 0;
    xmlXPathObjectPtr result;
    xmlNodeSetPtr nodeSet;
    for (i = 0; i < extractor->xpaths_len; i++) { // For every data field in the configuration
        tuple_t xpath = extractor->xpaths[i];
        result = xmlXPathEvalExpression((xmlChar*) xpath.value, context);
        nodeSet = result->nodesetval;
        if ( !xmlXPathNodeSetIsEmpty(nodeSet)) { // If found the field, extract the data
            char *text = nodeSet->nodeTab[0]->content;
            size_t len = strlen(text)+1;
            char* trimmed = malloc(len * sizeof(char));
            trimwhitespace(trimmed, len, text);
            cJSON_AddStringToObject(data, xpath.name, trimmed);
        } else { // Field not found -> Return error
            if (error == NULL) { // Create the error object if not already exists
                error = cJSON_CreateObject();
                cJSON_AddItemToObject(resp, "error", error);
                cJSON_AddStringToObject(error, "message", EXTRACTION_ERROR.message);
                cJSON_AddStringToObject(error, "type", EXTRACTION_ERROR.type);
                error_data = cJSON_CreateObject();
                cJSON_AddItemToObject(error, "data", error_data);
            }
            cJSON_AddStringToObject(error_data, xpath.name, "Not found. Please, check the URL and/or the configuration.");
        }
        xmlXPathFreeObject(result);
    }
    return resp;
}

// Build a serialized json response with a error message
char * build_error_msg(char* url, error_t error) {
    char *s = malloc(256*sizeof(char));
    sprintf(s, "{\"url\":\"%s\",\"error\":{\"type\":\"%s\",\"message\":\"%s\"}}", url, error.type, error.message);
    return s;
}

char * collect(char *url, extractors_t *extractors) {
    /* Get domain from URL */
    char *domain = get_domain(url);
    if (domain == NULL) return build_error_msg(url, INVALID_URL);

    /* Get extractor for domain */
    extractor_t* e = get_extractor(extractors, domain);
    if (e == NULL) return build_error_msg(url, UNKOWN_DOMAIN);

    /* Extract fields */
    string_t *content = malloc(sizeof(string_t));
    inistring_t(content);
    cJSON *r = NULL;
    if (get_html_page(url, content) == 200) {
        r = extract_json_data(url, content, e);
    } else {
        return build_error_msg(url, HTTP_NOT_OK);
    }

    /* Return serialized response */
    if (r != NULL) {
        char *s = cJSON_PrintUnformatted(r);
        cJSON_Delete(r);
        return s;
    } else {
        return build_error_msg(url, UNKOWN_ERROR);
    }
}

void console_mode(extractors_t *extractors, char *url) {
    char *r = collect(url, extractors);
    if (r != NULL) {
        printf("%s", r);
        free(r);
    } else {
        fprintf(stderr, "%s", build_error_msg(url, UNKOWN_ERROR));
    }
}

void socket_mode(extractors_t *extractors, unsigned short port) {
    int sockfd, newsockfd, clilen;
    char buffer[256];
    struct sockaddr_in serv_addr, cli_addr;
    int  n;

    /* First call to socket() function */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("ERROR opening socket");
        exit(EXIT_FAILURE);
    }

    /* Initialize socket structure */
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(port);
 
    /* Now bind the host address using bind() call.*/
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
         perror("ERROR on binding");
         exit(EXIT_FAILURE);
    }

    while(1) {
        /* Now start listening for the clients, here process will
        * go in sleep mode and will wait for the incoming connection
        */
        listen(sockfd, 5);
        clilen = sizeof(cli_addr);

        /* Accept actual connection from the client */
        newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, 
                                    &clilen);
        if (newsockfd < 0) {
            perror("ERROR on accept");
            continue;
        }
        /* If connection is established then start communicating */
        while(1) {
            memset(buffer, 0, 256);
            n = read(newsockfd, buffer, 255);

            /* Close connection if it had problem reading the socket */
            if (n <= 0) break;

            /* Perform data extraction */
            char *r = collect(buffer, extractors);

            /* Check for response NULL */
            if (r == NULL) r = build_error_msg(buffer, UNKOWN_ERROR);

            /* Write a response to the client */
            n = write(newsockfd,r,strlen(r));
            if (n < 0) {
                perror("ERROR writing to socket");
            }
            free(r);
        }
        close(newsockfd);
    }
}