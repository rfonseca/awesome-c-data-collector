Awesome C Data Collector
------------------------

Awesome C Data Collector or simply acdc, is a simple program written in C that extracts data from web pages using xpath.

## Build

Simply run:

    $ make

To build with debug symbols:

    $ make DEBUG=1

## External Libraries

These libraries are required to run the program and are commonly found on the repositories:

* [libxml](http://xmlsoft.org/index.html)
* [libcurl](http://curl.haxx.se/libcurl/)

These other external libraries are distributed together with the source code:

* [inih](http://code.google.com/p/inih/)
* [cJSON](https://sourceforge.net/projects/cjson/)
* [url_parser](http://draft.scyphus.co.jp/lang/c/url_parser.html)

## Dealing with errors

When some error occurs, an error object appended to the response object. The error object is composed by an 

Consider the following call:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.bash}
$ ./acdc -f test/acdc_broken.ini -u 'http://www.amazon.com.br/gp/product/B00E4PPIN6/' | python -mjson.tool
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.json}
{
    "data": {
        "price": "R$ 9,41",
        "title": "Goldfinger (007)"
    },
    "error": {
        "data": {
            "author": "Not found. Please, check the URL and/or the configuration."
        },
        "message": "One or more fields could not be extracted.",
        "type": "EXTRACTION_ERROR"
    },
    "url": "http://www.amazon.com.br/gp/product/B00E4PPIN6/"
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    $ ./acdc -u 'http://domain_unknow/etc'
    $ ./acdc -u 'some_invalid_url'