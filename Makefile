TARGET = acdc
_OBJ = $(TARGET).o cJSON.o ini.o url_parser.o

SDIR = src

CC = gcc
LIBS = -lcurl -lxml2 -lm
O = -O2
ifdef DEBUG
	O = -O0
endif
CFLAGS = -I/usr/include/libxml2 -I$(SDIR)

OBJ = $(patsubst %,$(SDIR)/%,$(_OBJ))

$(SDIR)/%.o: $(SDIR)/%.c $(SDIR)/%.h
	$(CC) -c -o $@ $< $(CFLAGS)

$(TARGET): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS) $(O)

$all: $(TARGET)

.PHONY: clean

clean:
	rm -rf $(TARGET) $(OBJ)
