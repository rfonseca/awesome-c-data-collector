#!/usr/bin/env python3

# Echo client program
import socket
import subprocess
from time import sleep

HOST = 'localhost'    # The remote host
PORT = 50007              # The same port as used by the server

links = [
	b'http://www.amazon.com.br/gp/product/B00A3D8VGO/',
	b'http://www.amazon.com.br/gp/product/B00E4PPIN6/',
	b'http://www.amazon.com.br/terras-devastadas-Torre-Negra-ebook/dp/B00A3D1D6Y/',
	b'http://www.amazon.com.br/Can%C3%A7%C3%A3o-Susannah-A-Torre-Negra-ebook/dp/B00BATIBHE/',
]

print('Starting acdc')
subprocess.Popen(['./acdc', '-s', str(PORT)])
sleep(1)

print('Collecting...')
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

print('Starting test:')
for link in links:

	s.sendall(link)
	data = s.recv(1024)
	print('start --->')
	print(repr(data))
	print('end   <---')

s.close()